@extends('layouts.master')
@section('content')
    <!-- Begin Content -->
    <div class="main-container with-padding">

        <!-- Begin Wrapper -->
        <div class="wrapper">

            <!-- Begin Inner Wrapper -->
            <div class="inner-wrapper">

                <!-- Begin Gallery -->
                <div class="gallery-images masonry style-icons high-space">
                    @if($showFilters->count() > 0)
                        <div class="row">

                            <div class="col full">

                                <div class="filters clearfix">

                                    <p>FILTERS</p>
                                    <a href="#" data-filter="*" class="active">ALL</a>
                                    @foreach($categories as $category)
                                        <a href="#" data-filter=".filter-{{$category->name}}">{{$category->name}}</a>
                                    @endforeach
                                </div>

                            </div>

                        </div>
                    @endif

                    <div class="images clearfix cols-4">
                        @if(count($portfolios) > 0)
                            @foreach($portfolios as $portfolio)
                                <div class="img filter-{{$portfolio->categoryDetail->name}} size-{{$portfolio->mainImageGallery->mode == 3 ? "regular" : "regular"}}">

                                    <div class="img-cont">
                                        <a href="{{Storage::url($portfolio->mainImageGallery->image_url)}}"
                                           class="preview-3"
                                           rel="gallery">
                                            <img class="lazy"
                                                 data-original="{{Storage::url($portfolio->mainImageGallery->image_thumbnail_url)}}"
                                                 alt=""
                                                 data-width="{{$portfolio->mainImageGallery->mode == 2 ? "475" : "475"}}"
                                                 data-height="{{$portfolio->mainImageGallery->mode == 1 ? "774" : "370"}}">
                                        </a>

                                    </div>

                                </div>
                            @endforeach
                        @else
                            <h1>No Portfolios Found</h1>
                        @endif


                    </div>

                </div>
                <!-- End Gallery -->

            </div>
            <!-- End Inner Wrapper -->

        </div>
        <!-- End Wrapper -->

    </div>
    <!-- End Content -->

@endsection