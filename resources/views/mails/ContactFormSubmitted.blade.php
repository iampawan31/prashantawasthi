<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New Contact Form Submitted</title>
</head>
<body>
<ul>

    <li><strong>Name:</strong> {{$name}}</li>
    <li><strong>Email:</strong> {{$email}}</li>
    <li><strong>Subject:</strong> {{$subject}}</li>
    <li><strong>Message:</strong> {{$message_content}}</li>
</ul>
</body>
</html>