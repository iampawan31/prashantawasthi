@extends('layouts.master')
@section('content')
    <div class="main-container">

        <div class="contact-1">

            <div class="map" data-address="New Delhi, India"></div>

            <a href="#" class="form-btn"></a>

            <div class="content-wrapper clearfix">

                <a href="#" class="close icon3-cross"></a>

                <div class="row">
                    <div class="col full">

                        <h4>CONTACT US</h4>

                    </div>
                </div>

                <div class="row">

                    <div class="info-box col one-third">
                        <p class="desc">PHONE</p>
                        <p class="info">+91 {{$data->phone_number}}</p>
                    </div>
                    <div class="info-box col one-third">
                        <p class="desc">EMAIL</p>
                        <p class="info">{{$data->email}}</p>

                    </div>
                    <div class="info-box col one-third">
                        <p class="desc">SOCIAL</p>
                        <p class="info">
                        <div class="social-icons contact-me">

                            <a href="{{$settings->facebook_url}}" class="icon1-facebook"></a>
                            <a href="{{$settings->instagram_url}}" class="icon1-instagram"></a>
                            <a href="{{$settings->twitter_url}}" class="icon1-twitter"></a>
                            {{--<a href="#" class="icon3-pinterest"></a>--}}

                        </div>
                        </p>
                    </div>

                </div>

                <div class="row">
                    <div class="col full">

                        <form method="post"
                              action="{{url('contact-form-post')}}">
                            {{csrf_field()}}
                            <input type="text" name="user_name" placeholder="NAME">
                            <input type="email" name="user_email" placeholder="EMAIL">
                            <input type="text" name="message_subject" placeholder="SUBJECT">
                            <textarea name="message_content" placeholder="MESSAGE"></textarea>
                            <input type="button" name="submit" value="SUBMIT">
                            <p class="message-info">Message Info.</p>
                        </form>

                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection