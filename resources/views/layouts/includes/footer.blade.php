<!-- Begin Footer -->
<footer>

    <p class="copyrights">© PRASHANT AWASTHI | ALL RIGHTS RESERVED</p>

    <div class="social-networks clearfix">
        <a href="{{$settings->facebook_url}}">
            <span>FACEBOOK</span>
            <i class="facebook-icon icon1-facebook"></i>
        </a>
        <a href="{{$settings->instagram_url}}">
            <span>INSTAGRAM</span>
            <i class="instagram-icon icon3-instagram"></i>
        </a>
        @if($settings->twitter_url != "#")
        <a href="{{$settings->twitter_url}}">
            <span>TWITTER</span>
            <i class="twitter-icon icon1-twitter"></i>
        </a>
        @endif
        {{--<a href="#">--}}
            {{--<span>PINTEREST</span>--}}
            {{--<i class="pinterest-icon icon3-pinterest"></i>--}}
        {{--</a>--}}

    </div>

</footer>
<!-- End Footer -->
<script type="text/javascript" src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/modernizr-respond.js')}}"></script>
<script type="text/javascript" src="{{asset('js/cookie.js')}}"></script>
<script type="text/javascript" src="{{asset('js/retina.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery-ui/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/scrollTo-min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{asset('js/appear.js')}}"></script>
<script type="text/javascript" src="{{asset('js/imagesloaded.pkgd.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jflickrfeed.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/flexslider/flexslider.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/isotope.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/queryloader2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/nicescroll.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/fitvids.js')}}"></script>
<script type="text/javascript" src="{{asset('js/Magnific-Popup/magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/mb.YTPlayer/inc/mb.YTPlayer.js')}}"></script>
<script type="text/javascript" src="{{asset('js/mousewheel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/lazyload.min.js')}}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwneOXi9gUfLXG25LLf9Edd6ekMVnaZec&callback=initMap"></script>
<script type="text/javascript" src="{{asset('js/scripts.js')}}"></script>

<script>
    $("img.lazy").lazyload();
</script>