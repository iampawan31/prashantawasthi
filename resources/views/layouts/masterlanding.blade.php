<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <title>{{$title}}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="description" content="Prashant Awasthi Photography"/>
    <meta name="keywords" content="Fashion, Modelling, Photography"/>
    <meta name="author" content="CodeSymbol"/>

    <link rel="icon" href="{{asset('favicon.ico')}}"/>

    <!-- Begin Stylesheets -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/reset.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('includes/entypo/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('includes/icomoon/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('includes/font_awesome/font-awesome.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('includes/cosy/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('js/jquery-ui/jquery-ui-1.10.3.custom.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('js/flexslider/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('js/Magnific-Popup/magnific-popup.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('js/mb.YTPlayer/css/YTPlayer.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- End Stylesheets -->


</head>
<body>
<div class="loader" data-background-color="#ffffff" data-text-color="#000000">
    <p>LOADING</p>
    <span class="circle"></span>
</div>
@yield('content')

<script type="text/javascript" src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/modernizr-respond.js')}}"></script>
<script type="text/javascript" src="{{asset('js/cookie.js')}}"></script>
<script type="text/javascript" src="{{asset('js/retina.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery-ui/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/scrollTo-min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{asset('js/appear.js')}}"></script>
<script type="text/javascript" src="{{asset('js/imagesloaded.pkgd.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jflickrfeed.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/flexslider/flexslider.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/isotope.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/queryloader2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/gmap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/nicescroll.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/fitvids.js')}}"></script>
<script type="text/javascript" src="{{asset('js/Magnific-Popup/magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/mb.YTPlayer/inc/mb.YTPlayer.js')}}"></script>
<script type="text/javascript" src="{{asset('js/mousewheel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/lazyload.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/scripts.js')}}"></script>

</body>
</html>