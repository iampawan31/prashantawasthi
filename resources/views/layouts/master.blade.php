<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>

    <meta charset="utf-8">
    <title>{{$title}}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="description" content="Prashant Awasthi Photography"/>
    <meta name="keywords" content="Fashion, Modelling, Photography"/>
    <meta name="author" content="CodeSymbol"/>

    <link rel="icon" href="{{asset('favicon.ico')}}"/>

    <!-- Begin Stylesheets -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/reset.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('includes/entypo/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('includes/icomoon/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('includes/font_awesome/font-awesome.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('includes/cosy/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('js/jquery-ui/jquery-ui-1.10.3.custom.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('js/flexslider/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('js/Magnific-Popup/magnific-popup.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('js/mb.YTPlayer/css/YTPlayer.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- End Stylesheets -->


</head>
<body>
<div class="loader" data-background-color="#ffffff" data-text-color="#000000">
    <p>LOADING</p>
    <span class="circle"></span>
</div>
<!-- Begin Header -->
<header>

    <a href="{{route('landing')}}" class="logo-container">
        <img src="{{Storage::url($settings->logo)}}">
    </a>

    <nav>
        <ul>
            <li>
                <a href="{{route('home')}}" class="{{url()->current() == route('home') ? 'active' : ''}}">HOME</a>
            </li>
            <li>
                <a href="{{route('portfolio')}}" class="{{url()->current() == route('portfolio') ? 'active' : ''}}">PORTFOLIO</a>
            </li>
            <li>
                <a href="{{route('personal')}}" class="{{url()->current() == route('personal') ? 'active' : ''}}">PERSONAL</a>
            </li>
            <li>
                <a href="{{route('about-me')}}" class="{{url()->current() == route('about-me') ? 'active' : ''}}">ABOUT ME</a>
            </li>
            <li>
                <a href="{{route('contact-me')}}" class="{{url()->current() == route('contact-me') ? 'active' : ''}}">CONTACT ME</a>
            </li>
        </ul>
    </nav>

    <a href="#" class="mobile-navigation icon3-list2"></a>

</header>
<!-- End Header -->
@yield('content')
@include('layouts.includes.footer')

</body>
</html>