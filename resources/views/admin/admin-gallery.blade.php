@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2 hidden-sm hidden-xs">
                @include('includes.sidebar')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="row">
                    @if (session('status'))
                        <div class="alert alert-danger">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <section>
                    <div class="panel panel-primary">
                        <div class="panel-heading">Image Upload</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <form action="{{route('dropzone-upload-image')}}" class="dropzone"
                                          id="my-awesome-dropzone">
                                        {{csrf_field()}}
                                    </form>
                                </div>
                                <div class="col-md-4">
                                    <ul class="list-group">
                                        <li class="list-group-item list-group-item-warning">Portrait(683px X 1024px)
                                        </li>
                                        <li class="list-group-item list-group-item-warning">Landscape(1920px X 1080px)
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="panel panel-primary">
                    <div class="panel-heading">Gallery</div>
                    <div class="panel-body">
                        @if(count($images) == 0)
                            <div class="alert alert-danger" role="alert">
                                You dont have any images.
                            </div>
                        @else
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Mode</th>
                                        <th>Added on</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($images as $image)
                                        <tr>
                                            <td>{{$image->id}}</td>
                                            <td>
                                                <div class="gallery-thumb-image"><img
                                                            src="{{Storage::url($image->image_thumbnail_url)}}"
                                                            alt=""></div>
                                            </td>
                                            <td>{{$image->original_name}}</td>
                                            <td>{{$image->mode == 1 ? "Portrait" : "Landscape"}}</td>
                                            <td>{{$image->created_at->diffForHumans()}}</td>
                                            <td><a href="{{url('admin-image-delete', [$image->id])}}"
                                                   class="btn btn-sm btn-danger">Delete</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="">
                                {{$images->links()}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection