@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2 hidden-sm hidden-xs">
                @include('includes.sidebar')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <a href="{{route('getPublicHomepage')}}" class="btn btn-danger btn-sm">Go Back</a>
                    </div>
                    <div class="panel-body">
                        <form action="{{route('admin-homepage-edit-order')}}" class="form" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="imageid" value="{{$image->id}}">
                            </div>
                            <div class="form-group">
                                <label for="order">Order</label>
                                <input type="text" class="form-control" name="order" value="{{$image->order}}">
                            </div>
                            <div class="form-group">
                                <label for="mode">Mode (Portrait: 683px X 1024px | Landscape: 1920px X
                                    1080px)</label>
                                <select name="mode" class="form-control" id="mode">
                                    <option value="1" {{$image->mode == 1 ? "selected" : ""}}>Portrait</option>
                                    <option value="2" {{$image->mode == 2 ? "selected" : ""}}>Landscape</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="thumbnail">
                            <img src="{{Storage::url($image->gallery->image_thumbnail_url)}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection