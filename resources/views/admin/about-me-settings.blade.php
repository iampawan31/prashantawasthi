@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2 hidden-sm hidden-xs">
                @include('includes.sidebar')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="row">
                    @if (session('status'))
                        <div class="alert alert-danger">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <section>

                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="{{route('postAboutSettings')}}" method="post">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="image">Select Profile Image</label>
                                            <select name="image" class="form-control" id="image">
                                                @foreach($gallery as $gal)
                                                    @if(count($aboutme) !=0)
                                                        <option value="{{$gal->id}}" {{$gal->id == $aboutme->aboutMeProfilePicture->id ? "selected" : ""}}>{{$gal->original_name}}</option>
                                                    @else
                                                        <option value="{{$gal->id}}">{{$gal->original_name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="image">Select Background Image</label>
                                            <select name="background-image" class="form-control" id="background-image">
                                                @foreach($gallery as $gal)
                                                    @if(count($aboutme) !=0)
                                                        <option value="{{$gal->id}}" {{$gal->id == $aboutme->aboutMeBackgroundPicture->id ? "selected" : ""}}>{{$gal->original_name}}
                                                            | {{ $gal->mode == 1 ? "Portrait" : "Landscape" }}</option>
                                                    @else
                                                        <option value="{{$gal->id}}">{{$gal->original_name}}
                                                            | {{ $gal->mode == 1 ? "Portrait" : "Landscape" }}</option>
                                                    @endif

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name"
                                                   value="{{count($aboutme) == 1 ? $aboutme->name : ""}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="place">Place</label>
                                            <input type="text" class="form-control" name="place"
                                                   value="{{count($aboutme) == 1 ? $aboutme->place : ""}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="work">Work</label>
                                            <input type="text" class="form-control" name="work"
                                                   value="{{count($aboutme) == 1 ? $aboutme->work: ""}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" class="form-control" name="title"
                                                   value="{{count($aboutme) == 1 ? $aboutme->title: ""}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">Phone Number</label>
                                            <input type="text" class="form-control" name="phone"
                                                   value="{{count($aboutme) == 1 ? $aboutme->phone_number: ""}}"
                                            >
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" name="email"
                                                   value="{{count($aboutme) == 1 ? $aboutme->email: ""}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="content">Content</label>
                                            <textarea name="content" class="form-control"
                                                      rows="3">{{count($aboutme) == 1 ? $aboutme->content: ""}}</textarea>
                                        </div>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <a href="" data-toggle="modal" data-target="#myModal"
                           class="btn btn-sm btn-danger"> {{count($aboutme) == 1 ? "Edit Profile" : "Add Profile"}}</a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-4">
                            @if(count($aboutme) == 1 )
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <strong>Name: </strong>{{$aboutme->name != "" ? $aboutme->name : "Not Specified!" }}
                                    </li>
                                    <li class="list-group-item">
                                        <strong>Place: </strong>{{$aboutme->place != "" ? $aboutme->place : "Not Specified!"}}
                                    </li>
                                    <li class="list-group-item">
                                        <strong>Work: </strong>{{$aboutme->work != "" ? $aboutme->work : "Not Specified!"}}
                                    </li>
                                    <li class="list-group-item">
                                        <strong>Title: </strong>{{$aboutme->title != "" ? $aboutme->title : "Not Specified!"}}
                                    </li>
                                    <li class="list-group-item"><strong>Phone
                                            Number: </strong>{{$aboutme->phone_number != "" ? $aboutme->phone_number : "Not Specified!"}}
                                    </li>
                                    <li class="list-group-item">
                                        <strong>Email: </strong>{{$aboutme->email != "" ? $aboutme->email : "Not Specified!"}}
                                    </li>
                                    <li class="list-group-item"><strong>About
                                            Me: </strong>{{$aboutme->content != "" ? $aboutme->content : "Not Specified!"}}
                                    </li>
                                </ul>
                            @else
                                <div class="alert alert-danger" role="alert">No Data Found</div>
                            @endif

                        </div>
                        <div class="col-md-4">
                            <div class="aboutme-thumbnail thumbnail">
                                @if(count($aboutme) == 1)
                                    <img src="{{Storage::url($aboutme->aboutMeProfilePicture->image_thumbnail_url)}}"
                                         alt="">
                                    <div class="caption">
                                        <h3>Profile Image</h3>
                                        <p>Image ID: {{$aboutme->aboutMeProfilePicture->id}}</p>
                                    </div>
                                @else
                                    <div class="alert alert-danger" role="alert">No Profile Image Found</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="aboutme-thumbnail thumbnail">
                                @if(count($aboutme) == 1)
                                    <img src="{{Storage::url($aboutme->aboutMeBackgroundPicture->image_thumbnail_url)}}"
                                         alt="">
                                    <div class="caption">
                                        <h3>Background Image</h3>
                                        <p>Image ID: {{$aboutme->aboutMeBackgroundPicture->id}}</p>
                                    </div>
                                @else
                                    <div class="alert alert-danger" role="alert">No Background Image Found</div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection