@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2 hidden-sm hidden-xs">
                @include('includes.sidebar')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <a href="{{route('getPortfolio')}}" class="btn btn-sm btn-danger">Go Back</a>
                    </div>
                    <div class="panel-body">
                        <form action="{{route('postPortfolioEditOrder')}}" class="form" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="imageid" value="{{$portfolio->id}}">
                            </div>
                            <div class="form-group">
                                <label for="category">Category</label>
                                <select name="category" id="category" class="form-control">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" {{$category->id == $portfolio->category ? "selected" : ""}}>
                                            {{$category->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="order">Order</label>
                                <input type="text" class="form-control" name="order"
                                       value="{{$portfolio->order == "" ? "" : $portfolio->order}}">
                            </div>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection