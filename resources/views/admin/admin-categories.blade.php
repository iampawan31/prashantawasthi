@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2 hidden-sm hidden-xs">
                @include('includes.sidebar')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="row">
                    @if (session('status'))
                        <div class="alert alert-danger">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Categories
                    </div>
                    <div class="panel-body">
                        @if(count($categories) == 0)
                            <div class="alert alert-danger" role="alert">
                                You dont have any landing page images set.
                            </div>
                        @else
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Mode</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr>
                                            <td>{{$category->id}}</td>
                                            <td>{{$category->name}}</td>
                                            <td>{{$category->type == 1 ? "Portfolio" : "Personal"}}</td>
                                            <td>{{$category->status == 1 ? "Enabled" : "Disabled"}}</td>
                                            <td>
                                                <a href="{{route('getCategoriesStatus', [$category->id, $category->status])}}"
                                                   class="btn btn-sm {{$category->status == 1? 'btn-danger' : 'btn-success'}}">{{ $category->status == 0 ? "Enable" : "Disable"}}</a>
                                                @if($category->status == 0)
                                                    <a href="{{route('getCategoriesDelete', [$category->id])}}"
                                                       class="btn btn-sm btn-danger">Delete</a>
                                                @endif</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="">
                                {{$categories->links()}}
                            </div>
                        @endif
                    </div>
                </div>
                </section>
            </div>
        </div>
    </div>
@endsection