@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2 hidden-sm hidden-xs">
                @include('includes.sidebar')
            </div>
            <div class="col-md-10 col-xs-12">
                <section>
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Add Image</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="{{ route('getLandingPage') }}" method="post">
                                        {{csrf_field()}}

                                        <div class="form-group">

                                        </div>
                                        <div class="form-group">
                                            <label for="image">Select Image</label>
                                            <select name="image" class="form-control image-picker" id="admin-image">
                                                @foreach($gallery as $gal)
                                                    <option value="{{$gal->id}}">ID: {{$gal->id}}
                                                        | {{$gal->original_name}}
                                                        | {{ $gal->mode == 1 ? "Portrait" : "Landscape" }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select name="status" class="form-control" id="status">
                                                <option value="enable">Enable</option>
                                                <option value="disable">Disable</option>
                                            </select>
                                        </div>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                    data-target="#myModal">
                                Add Image
                            </button>
                        </div>
                        <div class="panel-body">
                            @if(count($landingpage) == 0)
                                <div class="alert alert-danger" role="alert">
                                    You dont have any landing page images set.
                                </div>
                            @else
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Mode</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($landingpage as $land)
                                            <tr>
                                                <td>{{$land->gallery->id}}</td>
                                                <td>
                                                    <div class="gallery-thumb-image"><img
                                                                src="{{Storage::url($land->gallery->image_thumbnail_url)}}"
                                                                alt=""></div>
                                                </td>
                                                <td>{{$land->gallery->original_name}}</td>
                                                <td>{{$land->gallery->mode == 1 ? "Portrait" : "Landscape"}}</td>
                                                <td>{{$land->status == 1 ? "Enabled" : "Disabled"}}</td>
                                                <td>
                                                    <a href="{{route('getLandingPageStatus', [$land->id, $land->status, 1])}}"
                                                       class="btn btn-sm {{$land->status == 1? 'btn-danger' : 'btn-success'}}">{{ $land->status == 0 ? "Enable" : "Disable"}}</a>
                                                    @if($land->status == 0)
                                                        <a href="{{route('getLandingPageDelete', [$land->id, 1])}}"
                                                           class="btn btn-sm btn-danger">Delete</a>
                                                    @endif</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="">
                                    {{$landingpage->links()}}
                                </div>
                            @endif
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection