@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2 hidden-sm hidden-xs">
                @include('includes.sidebar')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="row">
                    @if (session('status'))
                        <div class="alert alert-danger">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">Contact Form Submissions</div>
                    <div class="panel-body">
                        @if(count($contactforms) == 0)
                            <div class="alert alert-danger" role="alert">
                                You dont have form submissions.
                            </div>
                        @else
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Subject</th>
                                        <th>Message</th>
                                        <th>Submitted On</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($contactforms as $form)
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{$form->name}}</td>
                                            <td>{{$form->email}}</td>
                                            <td>{{$form->subject}}</td>
                                            <td>{{$form->message_content}}</td>
                                            <td>{{$form->created_at->diffForHumans()}}</td>
                                            <td><a href="{{url('admin-contact', [$form->id])}}"
                                                   class="btn btn-sm btn-danger">Delete</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="">
                                {{$contactforms->links()}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection