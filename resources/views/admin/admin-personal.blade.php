@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2 hidden-sm hidden-xs">
                @include('includes.sidebar')
            </div>
            <div class="col-md-10 col-xs-12">
                <div class="row">
                    @if (session('status'))
                        <div class="alert alert-danger">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <section>
                    <!-- Modal -->
                    <div class="modal fade" id="categoryModal" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Add Portfolio Category</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="{{route('admin-personal-category')}}" method="post">
                                        {{csrf_field()}}

                                        <div class="form-group">
                                            <label for="name">Category Name</label>
                                            <input type="text" name="name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select name="status" class="form-control" id="status">
                                                <option value="1">Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Add Personal Portfolio</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="{{route('postPersonal')}}" method="post">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" name="name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <select name="category" class="form-control" id="category">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">
                                                        {{$category->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{--<div class="form-group">--}}
                                        {{--<label for="image">Select Cover</label>--}}
                                        {{--<select name="cover" class="form-control" id="cover">--}}
                                        {{--@foreach($gallery as $gal)--}}
                                        {{--<option value="{{$gal->id}}">{{$gal->original_name}}--}}
                                        {{--(<strong>ID:</strong>{{$gal->id}})--}}
                                        {{--</option>--}}
                                        {{--@endforeach--}}
                                        {{--</select>--}}
                                        {{--</div>--}}
                                        <div class="form-group">
                                            <label for="image">Select Image</label>
                                            <select name="image" class="form-control" id="image">
                                                @foreach($gallery as $gal)
                                                    <option value="{{$gal->id}}">{{$gal->original_name}}
                                                        | {{ $gal->mode == 1 ? "Portrait" : "Landscape" }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select name="status" class="form-control" id="status">
                                                <option value="1">Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="status">Order</label>
                                            <input type="text" name="order" class="form-control">
                                        </div>

                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" class="btn btn-danger btn-sm " data-toggle="modal"
                                    data-target="#myModal">
                                Add Portfolio
                            </button>
                            <button type="button" class="btn btn-danger btn-sm " data-toggle="modal"
                                    data-target="#categoryModal">
                                Add Category
                            </button>
                        </div>
                        <div class="panel-body">
                            @if(count($personal) == 0)
                                <div class="alert alert-danger" role="alert">
                                    You don't have any Personal Images.
                                </div>
                            @else
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Order</th>
                                            <th>Mode</th>
                                            <th>Status</th>
                                            <th>Category</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($personal as $item)
                                            <tr>
                                                <td>{{$item->id}}</td>
                                                <td>
                                                    <div class="gallery-thumb-image"><img
                                                                src="{{Storage::url($item->mainImageGallery->image_thumbnail_url)}}"
                                                                alt=""></div>
                                                </td>
                                                <td>{{$item->title}}</td>
                                                <td>{{$item->order}}</td>
                                                <td>{{$item->status}}</td>
                                                <td>{{$item->mainImageGallery->mode == 1 ? "Portrait" : "Landscape"}}</td>
                                                <td>{{$item->categoryDetail->name}}</td>
                                                <td><a href="{{route('getPersonalEditOrder', [$item->id])}}"
                                                       class="btn btn-sm btn-warning">Edit</a>
                                                        <a href="{{route('getLandingPageDelete', [$item->id, 3])}}"
                                                           class="btn btn-sm btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="">
                                    {{$personal->links()}}
                                </div>
                            @endif
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection