@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2 hidden-sm hidden-xs">
                @include('includes.sidebar')
            </div>
            <div class="col-md-10 col-xs-12">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">{{ session('status') }}</div>
                @endif

                @if(count($settings) == 1)
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Basic Settings</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4>Website Logo</h4>
                                    <div class="thumbnail">
                                        <img src="{{Storage::url($settings->logo)}}" alt="Logo">
                                    </div>
                                    <a id="settingsButton" class="btn btn-warning btn-sm" data-toggle="modal"
                                            data-target="#myModal">
                                        Change Logo
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <h4>Website Title</h4>
                                    <p>{{$settings->website_title}}</p>
                                    <p>
                                        <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                                data-target="#myModal">
                                            Edit
                                        </button>
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <h4>New Registeration</h4>
                                    <p>{{$settings->registration_allowed == "1" ? "Allowed" : "Not Allowed"}}</p>
                                    <p>
                                        <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                                data-target="#myModal">
                                            Edit
                                        </button>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <h4>Portfolio Order</h4>
                                    <p>{{ $order[$settings->portfolio_order] }}</p>
                                </div>
                                <div class="col-md-4">
                                    <h4>Personal Order</h4>
                                    <p>{{ $order[$settings->personal_order] }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Social Media Links Settings</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Link</th>
                                        <th>Enabled</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="{{$settings->facebook_url == "#" ? 'danger' : 'success'}}">
                                        <td>Facebook</td>
                                        <td>
                                            <a href="{{$settings->facebook_url}}"
                                               target="_blank">{{$settings->facebook_url}}</a>
                                        </td>
                                        <td>{{$settings->facebook_url == "#" ? "Disabled" : "Enabled"}}</td>
                                        <td>
                                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                                    data-target="#myModal">
                                                Edit
                                            </button>
                                        </td>
                                    </tr>
                                    <tr class="{{$settings->instagram_url == "#" ? 'danger' : 'success'}}">
                                        <td>Instagram</td>
                                        <td>
                                            <a href="{{$settings->instagram_url}}"
                                               target="_blank">{{$settings->instagram_url}}</a>
                                        </td>
                                        <td>{{$settings->instagram_url == "#" ? "Disabled" : "Enabled"}}</td>
                                        <td>
                                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                                    data-target="#myModal">
                                                Edit
                                            </button>
                                        </td>
                                    </tr>
                                    <tr class="{{$settings->twitter_url == "#" ? 'danger' : 'success'}}">
                                        <td>Twitter</td>
                                        <td>
                                            <a href="{{$settings->twitter_url}}"
                                               target="_blank">{{$settings->twitter_url}}</a>
                                        </td>
                                        <td>{{$settings->twitter_url == "#" ? "Disabled" : "Enabled"}}</td>
                                        <td>
                                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                                    data-target="#myModal">
                                                Edit
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            @else
                <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
                        Add Settings
                    </button>
            @endif
            <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Settings</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('postAdminHome') }}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="logo">Website Logo</label>
                                        <input type="file" name="logo" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Website Title</label>
                                        <input type="text" name="title" class="form-control"
                                               value="{{count($settings) == 0 ? "" : $settings->website_title}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="facebook_url">Facebook Link</label>
                                        <input type="text" name="facebook_url" class="form-control"
                                               value="{{count($settings) == 0 ? "" : $settings->facebook_url}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="facebook_url">Instagram Link</label>
                                        <input type="text" name="instagram_url" class="form-control"
                                               value="{{count($settings) == 0 ? "" : $settings->instagram_url}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="facebook_url">Twitter Link</label>
                                        <input type="text" name="twitter_url" class="form-control"
                                               value="{{count($settings) == 0 ? "" : $settings->twitter_url}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="facebook_url">Registeration Allowed?</label>
                                        <select name="registration_allowed" class="form-control">
                                            <option value="1" selected>Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Portfolio Image Order</label>
                                        <select name="portfolio_order" class="form-control" id="status">
                                            <option value="1">Name (Asc)</option>
                                            <option value="2">Name (Desc)</option>
                                            <option value="3">Order (Asc)</option>
                                            <option value="4">Order (Desc)</option>
                                            <option value="5">Uploaded (Asc)</option>
                                            <option value="6">Uploaded (Desc)</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Personal Image Order</label>
                                        <select name="personal_order" class="form-control" id="status">
                                            <option value="1">Name (Asc)</option>
                                            <option value="2">Name (Desc)</option>
                                            <option value="3">Order (Asc)</option>
                                            <option value="4">Order (Desc)</option>
                                            <option value="5">Uploaded (Asc)</option>
                                            <option value="6">Uploaded (Desc)</option>
                                        </select>
                                    </div>

                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection