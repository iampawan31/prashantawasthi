<div class="list-group" data-spy="affix" data-offset-top="10" data-offset-bottom="200">
    <a class="list-group-item {{url()->current() == route('getAdminHome') ? 'list-group-item active' : ''}}"
       href="{{route('getAdminHome')}}">Settings</a>
    <a class="list-group-item {{ url()->current() == route('getLandingPage') ? 'list-group-item active' : '' }}"
       href="{{ route('getLandingPage') }}">Landing</a></li>
    <a class="list-group-item {{ url()->current() == route('getPublicHomepage') ? 'list-group-item active' : '' }}"
       href="{{ route('getPublicHomepage') }}">Home</a>
    <a class="list-group-item {{ url()->current() == route('getGallery') ? 'list-group-item active' : '' }}"
       href="{{ route('getGallery') }}">Gallery</a>
    <a class="list-group-item {{ url()->current() == route('getPersonal') ? 'list-group-item active' : '' }}"
       href="{{ route('getPersonal') }}">Personal</a>
    <a class="list-group-item {{ url()->current() == route('getPortfolio') ? 'list-group-item active' : '' }}"
       href="{{ route('getPortfolio') }}">Portfolio</a>
    <a class="list-group-item {{ url()->current() == route('getAboutSettings') ? 'list-group-item active' : '' }}"
       href="{{ route('getAboutSettings') }}">About Me</a>
    <a class="list-group-item {{ url()->current() == route('getContact') ? 'list-group-item active' : '' }}"
       href="{{ route('getContact') }}">Contact Form</a>
    <a class="list-group-item {{ url()->current() == route('getCategories') ? 'list-group-item active' : '' }}"
       href="{{ route('getCategories') }}">Categories</a>
</div>