@extends('layouts.master')
@section('content')
        <!-- Begin Content -->
<div class="main-container with-padding">

    <!-- Begin Wrapper -->
    <div class="wrapper">

        <!-- Begin Horizontal Gallery -->
        <div class="gallery-h normal">

            <div class="nav">
                <a href="#" class="prev icon4-leftarrow23"></a>
                <a href="#" class="next icon4-chevrons"></a>
            </div>

            <div class="gallery clearfix">

                <div class="container">
                    @if(count($homepage) == 0)
                        <h1>No Images Found</h1>
                    @else
                        @foreach($homepage as $home)
                            <div class="img {{floor($loop->count/2) == $loop->index? "active" : ""}}">
                                <a href="{{Storage::url($home->gallery->image_url)}}" rel="gallery">
                                    <img class="lazy"
                                         data-original="{{Storage::url($home->gallery->image_url)}}" alt=""
                                         data-width="{{$home->mode == 1 ? "683" : "1920"}}"
                                         data-height="{{$home->mode == 1 ? "1024" : "1080"}}">
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>

            </div>

        </div>
        <!-- End Horizontal Gallery -->

    </div>
    <!-- End Wrapper -->

</div>
<!-- End Content -->

@endsection