@extends('layouts.masterlanding')
@section('content')

        <!-- Begin Content -->
<div class="landing-slideshow">

    <!-- Begin Logo container -->
    <div class="logo-container">

        <a href="{{route('home')}}" class="source">

            <img src="{{Storage::url($settings->logo)}}" alt="">

                <span class="animate">
                    <img src="{{Storage::url($settings->logo)}}" alt="">
                </span>

        </a>
        <a href="{{route('home')}}" class="enter-now-button">ENTER NOW</a>

    </div>
    <!-- End Logo container -->


    <!-- Begin Slides -->
    <div class="flexslider">
        <ul class="slides">
            @if(count($landingpages) == 0)
                <h1>No Images Found</h1>
            @else
                @foreach($landingpages as $landing)
                    @if($landing->status)
                        <li style="background-image:url({{Storage::url($landing->gallery->image_url)}})"></li>
                    @endif
                @endforeach
            @endif
        </ul>
    </div>
    <!-- End Slides -->

</div>
<!-- End Content -->

@endsection