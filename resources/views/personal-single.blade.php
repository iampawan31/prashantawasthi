@extends('layouts.master')
@section('content')
        <!-- Begin Content -->
<div class="main-container with-padding">

    <!-- Begin Wrapper -->
    <div class="wrapper">

        <!-- Begin Portfolio -->
        <div class="portfolio-single style-1">

            <!-- Begin Navigation -->
            <div class="nav">
                <a href="#" class="prev icon4-leftarrow23"></a>
                <a href="#" class="next icon4-chevrons"></a>
            </div>
            <!-- End Navigation -->

            <!-- Begin Inner Wrapper -->
            <div class="row inner-wrapper">

                <div class="col full content clearfix">

                    <div class="images">
                        @foreach($personal->personalGallery as $image)

                            <a href="{{Storage::url($image->gallery->image_url)}}" rel="gallery">
                                <img class="lazy" data-original="{{Storage::url($image->gallery->image_url)}}" alt=""
                                     data-width="1365"
                                     data-height="2048">
                            </a>
                        @endforeach
                    </div>

                </div>

            </div>
            <!-- End Inner Wrapper -->

        </div>
        <!-- End Portfolio -->

    </div>
    <!-- End Wrapper -->

</div>
<!-- End Content -->

@endsection