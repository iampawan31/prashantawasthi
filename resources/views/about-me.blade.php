@extends('layouts.master')
@section('content')
    <div class="main-container"
         style="background-image:url({{Storage::url($data->aboutMeBackgroundPicture->image_url)}})">

        <div class="min-style">
            <div class="content-wrapper clearfix">

                <div class="row">

                    <div class="col three-fourth">
                        @if($data->name != "")
                            <div class="info-box col one-fourth">
                                <p class="desc">NAME</p>
                                <p class="info">{{$data->name != "" ? $data->name : ""}}</p>
                            </div>
                        @endif
                        @if($data->place != "")
                            <div class="info-box col one-fourth">
                                <p class="desc">PLACE</p>
                                <p class="info">{{$data->place != "" ? $data->place : ""}}</p>
                            </div>
                        @endif
                        @if($data->work != "")
                            <div class="info-box col one-fourth">
                                <p class="desc">WORK</p>
                                <p class="info">{{$data->work != "" ? $data->work : ""}}</p>
                            </div>
                        @endif
                        @if($data->title != "")
                            <div class="info-box col one-fourth">
                                <p class="desc">TITLE</p>
                                <p class="info">{{$data->title != "" ? $data->title : ""}}</p>
                            </div>
                        @endif

                        <div class="row clearfix">
                            <div class="col full about-me-text" style="color:#000000;">

                                <p>{{$data->content}}</p>

                                <div class="divider" style="height:20px;"></div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col full">
                                @if($data->phone_number != "")
                                    <div class="phone"><p>
                                            +91 {{$data->phone_number != "" ? $data->phone_number : ""}}</p>
                                    </div>
                                @endif

                                <div class="social-icons">

                                    <a href="{{$settings->facebook_url}}" class="icon1-facebook"></a>
                                    <a href="{{$settings->instagram_url}}" class="icon1-instagram"></a>
                                    <a href="{{$settings->twitter_url}}" class="icon1-twitter"></a>
                                    {{--                                    <a href="{{$settings->facebook_url}}" class="icon3-pinterest"></a>--}}


                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="col one-fourth">
                        <img class="personal-img" src="{{Storage::url($data->aboutMeProfilePicture->image_url)}}"
                             alt="">
                    </div>

                </div>

                <div class="clear"></div>

            </div>
        </div>

    </div>

@endsection