<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StaticControllerTest extends TestCase
{
	use DatabaseMigrations;
	use WithoutMiddleware;
    use DatabaseTransactions;

    public function test_index_method_returns_landing_page()
    {
    	$globalSettings = factory(App\GlobalSetting::class)->create();
    	
    	$this->visit('/')
            ->assertViewHas('settings')
            ->seePageIs('/');

    }

    public function test_visit_landing_page_and_click_enter_now()
    {
        $globalSettings = factory(App\GlobalSetting::class)->create();

        $this->visit('/')
            ->assertViewHas('settings')
            ->click('ENTER NOW')
            ->seePageIs('/home')
            ->assertViewHas('settings');
    }

}
