<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminControllerTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseMigrations;
    use DatabaseTransactions;

    public function test_visit_admin_settings_page()
    {
        $globalSettings = factory(App\GlobalSetting::class)->create();

        $this->visit('admin')
            ->assertViewHas('settings')
            ->seePageIs('admin');
    }

    public function test_visit_admin_settings_page_and_update_the_settings()
    {
        $globalSettings = factory(App\GlobalSetting::class)->create();

        $this->visit('admin')
            ->assertViewHas('settings')
            ->click('Change Logo')
            ->type('Pawan Kumar', 'title')
            ->type('http://www.facebook.com', 'facebook_url')
            ->type('http://www.facebook.com', 'instagram_url')
            ->type('http://www.facebook.com', 'twitter_url')
            ->select('0', 'registration_allowed')
            ->press('Save changes')
            ->seePageIs('admin')
            ->assertSessionHas('status');
    }
}
