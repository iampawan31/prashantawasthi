<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/', 'StaticController@index')->name('landing');
Route::get('home', 'StaticController@getHome')->name('home');
Route::get('about', 'StaticController@getAboutMe')->name('about-me');
Route::get('contact', 'StaticController@getContactMe')->name('contact-me');
Route::get('personal', 'StaticController@getPersonal')->name('personal');
Route::get('portfolio', 'StaticController@getPortfolio')->name('portfolio');
Route::get('personal/{id}', 'StaticController@getSinglePersonal');
Route::post('contact-form-post', 'StaticController@postContactForm');
Auth::routes();


//Admin Routes
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('getAdminHome');
    Route::post('/', 'AdminController@postIndex')->name('postAdminHome');

    Route::get('about-me', 'AdminController@getAboutMeSettings')->name('getAboutSettings');
    Route::post('about-me', 'AdminController@postAboutMeSettings')->name('postAboutSettings');

    Route::get('gallery', 'AdminController@getAdminGallery')->name('getGallery');
    Route::post('upload-image', 'AdminController@postUploadImage')->name('postUploadImage');
    Route::get('gallery/{imageid}', 'AdminController@postDeleteGalleryImage');

    Route::get('homepage', 'AdminController@getHomePage')->name('getPublicHomepage');
    Route::post('homepage', 'AdminController@postHomePage')->name('postPublicHomepage');

    Route::get('portfolio', 'AdminController@getPortfolio')->name('getPortfolio');
    Route::post('portfolio', 'AdminController@postPortfolio')->name('postPortfolio');

    Route::get('personal', 'AdminController@getPersonal')->name('getPersonal');
    Route::post('personal', 'AdminController@postPersonal')->name('postPersonal');

    Route::get('contact', 'AdminController@getContact')->name('getContact');
    Route::get('contact/{formid}', 'AdminController@postContact');

    Route::get('landing-page', 'AdminController@getLandingPage')->name('getLandingPage');
    Route::post('landing-page', 'AdminController@postLandingPage')->name('postLandingPage');

    Route::get('landing-page-status/{imageid}/{status}/{type}', 'AdminController@postStatusChange')->name('getLandingPageStatus');
    Route::get('landing-page-delete/{imageid}/{type}', 'AdminController@postDeleteImage')->name('getLandingPageDelete');

    Route::get('homepage-status/{imageid}/{status}/{type}', 'AdminController@postStatusChange')->name('getHomepageStatus');
    Route::get('homepage-delete/{imageid}/{type}', 'AdminController@postDeleteImage')->name('getHomepageDelete');

    Route::get('homepage-edit-order/{id}', 'AdminController@getAdminHomepageEditOrder')->name('getHomepageEditOrder');
    Route::post('homepage-edit-order', 'AdminController@postAdminHomepageEditOrder')->name('admin-homepage-edit-order');

    Route::get('portfolio-edit-order/{id}', 'AdminController@getAdminPortfolioEditOrder')->name('getPortfolioEditOrder');
    Route::post('portfolio-edit-order', 'AdminController@postAdminPortfolioEditOrder')->name('postPortfolioEditOrder');

    Route::get('personal-edit-order/{id}', 'AdminController@getAdminPersonalEditOrder')->name('getPersonalEditOrder');
    Route::post('personal-edit-order', 'AdminController@postAdminPersonalEditOrder')->name('postPersonalEditOrder');


    Route::post('portfolio-category', 'AdminController@postAdminPortfolioCategory')->name('admin-portfolio-category');
    Route::post('personal-category', 'AdminController@postAdminPersonalCategory')->name('admin-personal-category');

    Route::post('admin-image-upload', 'AdminController@uploadImage')->name('dropzone-upload-image');

    Route::get('categories', 'AdminController@getCategories')->name('getCategories');
    Route::get('admin-categories-status/{categoryid}/{status}', 'AdminController@postStatusChangeCategory')->name('getCategoriesStatus');
    Route::get('admin-categories-delete/{categoryid}', 'AdminController@postDeleteCategory')->name('getCategoriesDelete');

});

