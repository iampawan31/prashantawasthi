<?php

$factory->define(App\GlobalSetting::class, function (Faker\Generator $faker) {

	return [
		'logo' => $faker->imageUrl(),
		'website_title' => $faker->title(),
		'facebook_url' => $faker->url(),
		'instagram_url' => $faker->url(),
		'twitter_url' => '#',
		'registration_allowed' => $faker->boolean()
	];
});