<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandingPage extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function gallery(){
        return $this->belongsTo(Gallery::class, 'image_id', 'id');
    }
}
