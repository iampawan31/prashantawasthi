<?php

namespace App\Http\Controllers;

use App\AboutMe;
use App\Category;
use App\Events\ContactFormSubmitted;
use App\GlobalSetting;
use App\HomepageSetting;
use App\LandingPage;
use App\Personal;
use App\Portfolio;
use Cache;
use Illuminate\Http\Request;

/**
 * Class StaticController
 * @package App\Http\Controllers
 */
class StaticController extends Controller
{

    /*
         * 1. Name (ASC)
           2. Name (DESC)
           3. Order (ASC)
           4. Order (DESC)
           5. Uploaded (ASC)
           6. Uploaded (DESC)
        */

    private $orderArrayType;
    private $orderArrayDirection;

    public function __construct()
    {
        $this->orderArrayType = array(
            '1' => 'title',
            '2' => 'title',
            '3' => 'order',
            '4' => 'order',
            '5' => 'created_at',
            '6' => 'created_at',
        );

        $this->orderArrayDirection = array(
            '1' => 'asc',
            '2' => 'desc',
            '3' => 'asc',
            '4' => 'desc',
            '5' => 'asc',
            '6' => 'desc',
        );
    }


    /**
     * @return $this
     */
    public function index()
    {
        $landing = LandingPage::all();
        $settings = GlobalSetting::first();
        return view('landing')->with([
            'title' => $this->getTitle("Landing", $settings->website_title),
            'landingpages' => $landing,
            'settings' => $settings
        ]);
    }

    /**
     * @return $this
     */
    public function getHome()
    {
        $homepage = HomepageSetting::orderBy('order', 'asc')->get();
        $settings = GlobalSetting::first();

        return view('home')->with([
            'title' => $this->getTitle("Home", $settings->website_title),
            'homepage' => $homepage,
            'settings' => $settings
        ]);
    }

    /**
     * @return $this
     */
    public function getAboutMe()
    {
        $data = AboutMe::first();
        $settings = GlobalSetting::first();

        return view('about-me')->with([
            'title' => $this->getTitle("About Me", $settings->website_title),
            'data' => $data,
            'settings' => $settings
        ]);
    }

    /**
     * @return $this
     */
    public function getContactMe()
    {
        $settings = GlobalSetting::first();
        $data = AboutMe::first();

        return view('contact-me')->with([
            'title' => $this->getTitle("Contact Me", $settings->website_title),
            'data' => $data,
            'settings' => $settings
        ]);
    }


    /**
     * @return $this
     */
    public function getPersonal()
    {
        $settings = GlobalSetting::first();

        $personal = Personal::orderBy($this->orderArrayType[$settings->personal_order], $this->orderArrayDirection[$settings->personal_order])->get();
        $categories = Category::where('type', 2)->orderBy('name', 'asc')->get();

        $showFilters = $categories->reject(function ($value) {
            return $value->status == 0;
        });


        return view('personal')->with([
            'title' => $this->getTitle("Portfolio", $settings->website_title),
            'personal' => $personal,
            'categories' => $categories,
            'settings' => $settings,
            'showFilters' => $showFilters
        ]);
    }

    /**
     * @return $this
     */
    public function getPortfolio()
    {
        $settings = GlobalSetting::first();
        $portfolios = Portfolio::orderBy($this->orderArrayType[$settings->portfolio_order], $this->orderArrayDirection[$settings->portfolio_order])->get();
        $categories = Category::where('type', 1)->orderBy('name', 'asc')->get();

        $showFilters = $categories->reject(function ($value) {
            return $value->status == 0;
        });

        return view('portfolio')->with([
            'title' => $this->getTitle("Portfolio", $settings->website_title),
            'portfolios' => $portfolios,
            'categories' => $categories,
            'settings' => $settings,
            'showFilters' => $showFilters
        ]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function getSinglePersonal($id)
    {
        $personal = Personal::find($id);
        $settings = GlobalSetting::first();

        if (count($personal) == 0) {
            abort(404, "Not Found");
        } else {
            return view('personal-single')->with([
                'title' => $this->getTitle("Personal", $settings->website_title),
                'personal' => $personal,
                'settings' => $settings
            ]);
        }
    }

    public function postContactForm(Request $request)
    {
        event(new ContactFormSubmitted($request));
        return "success";
    }


    /**
     * @param $pageTitle
     * @param string $settings
     * @return string
     */
    private function getTitle($pageTitle, $settings = "Prashant Awasthi Photography")
    {
        return title_case($settings) . " | " . $pageTitle;
    }
}
