<?php

namespace App\Http\Controllers;

use App\AboutMe;
use App\Category;
use App\ContactForm;
use App\Gallery;
use App\GlobalSetting;
use App\HomepageSetting;
use App\Http\Requests;
use App\LandingPage;
use App\Personal;
use App\PersonalGallery;
use App\Portfolio;
use App\PortfolioGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;


/**
 * Class AdminController
 * @package App\Http\Controllers
 */
class AdminController extends Controller
{
    private $orderArray;
    public function __construct()
    {
        $this->orderArray = array(
            '1' => 'Title (ASC)',
            '2' => 'Title (DESC)',
            '3' => 'Order (ASC)',
            '4' => 'Order (DESC)',
            '5' => 'Uploaded (ASC)',
            '6' => 'Uploaded (DESC)',
        );
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $globalSettings = GlobalSetting::first();

        return view('admin.adminhome')->with([
            'settings' => $globalSettings,
            'order' => $this->orderArray
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAboutMeSettings()
    {
        $aboutme = AboutMe::first();
        $gallery = $this->getGallery();

        return view('admin.about-me-settings')->with([
            'aboutme' => $aboutme,
            'gallery' => $gallery
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdminGallery()
    {
        $images = Gallery::paginate(25);

        return view('admin.admin-gallery')->with('images', $images);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getHomePage()
    {
        $homepagesettings = HomepageSetting::paginate(10);
        $gallery = $this->getGallery();

        return view('admin.admin-homepage')->with([
            'gallery' => $gallery,
            'homepagesettings' => $homepagesettings
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLandingPage()
    {
        $landingpage = LandingPage::paginate(10);
        $gallery = $this->getGallery();

        return view('admin.admin-landingpage')->with([
            'landingpage' => $landingpage,
            'gallery' => $gallery->where('mode', 2)
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPortfolio()
    {
        $gallery = $this->getGallery();
        $categories = Category::where('type', 1)->get();
        $portfolio = Portfolio::paginate(10);

//        $portfoliocategory = $this->getGallery()->filter(function ($item) {
//            return ($item->mode == 3 || $item->mode == 4);
//        });

        return view('admin.admin-portfolio')->with([
            'gallery' => $gallery,
            'categories' => $categories,
            'portfolios' => $portfolio
//            'portfoliocategory' => $portfoliocategory
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPersonal()
    {
//        $cover = Gallery::where('mode', 3)->orWhere('mode', 4)->get();
        $gallery = Gallery::where('mode', 1)->orWhere('mode', 2)->get();
        $categories = Category::where('type', 2)->get();
        $personal = Personal::paginate(10);

        return view('admin.admin-personal')->with([
            'gallery' => $gallery,
            'categories' => $categories,
            'personal' => $personal
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdminHomepageEditOrder(Request $request, $id)
    {
        $image = HomepageSetting::find($id);

        return view('admin.admin-homepage-edit-order')->with('image', $image);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdminPortfolioEditOrder(Request $request, $id)
    {
        $portfolio = Portfolio::find($id);
        $categories = Category::where('type', '1')->get();

        return view('admin.admin-portfolio-edit-order')->with([
            'portfolio' => $portfolio,
            'categories' => $categories
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function getAdminPersonalEditOrder(Request $request, $id)
    {
        $personal = Personal::find($id);
        $categories = Category::where('type', '2')->get();

        return view('admin.admin-personal-edit-order')->with([
            'personal' => $personal,
            'categories' => $categories
        ]);
    }

    /**
     * @return $this
     */
    public function getContact()
    {
        $contactForm = ContactForm::paginate(10);

        return view('admin.admin-contact')->with('contactforms', $contactForm);
    }

    /**
     * @return $this
     */
    public function getCategories()
    {
        $categories = Category::paginate(10);

        return view('admin.admin-categories')->with('categories', $categories);
    }

    /**
     * @param $formId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postContact($formId)
    {
        $contactForm = ContactForm::find($formId);
        $contactForm->delete();
        return redirect('admin/contact')->with('status', 'Contact Form submission deleted successfully');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex(Request $request)
    {
        if ($request->hasFile('logo')) {
            $temp = GlobalSetting::first();
            if (count($temp) == 0) {
                $path = $request->file('logo')->store('gallery');
                $globalSetting = new GlobalSetting();
                $globalSetting->logo = $path;
                $globalSetting->website_title = $request->title;
                $globalSetting->facebook_url = $request->facebook_url;
                $globalSetting->instagram_url = $request->instagram_url;
                $globalSetting->twitter_url = $request->twitter_url;
                $globalSetting->registration_allowed = $request->registration_allowed == "1" ? true : false;
                $globalSetting->portfolio_order = $request->portfolio_order;
                $globalSetting->personal_order = $request->personal_order;
                $globalSetting->save();

                return redirect('admin')->with('status', 'Settings Added Successfully');
            } else {
                Storage::delete($temp->logo);
                $path = $request->file('logo')->store('gallery');
                $temp->logo = $path;
                $temp->website_title = $request->title;
                $temp->facebook_url = $request->facebook_url;
                $temp->instagram_url = $request->instagram_url;
                $temp->twitter_url = $request->twitter_url;
                $temp->registration_allowed = $request->registration_allowed == "1" ? true : false;
                $temp->portfolio_order = $request->portfolio_order;
                $temp->personal_order = $request->personal_order;
                $temp->update();

                return redirect('admin')->with('status', 'Settings Updated Successfully');
            }
        } else {
            $temp = GlobalSetting::first();
            if (count($temp) == 0) {
                $globalSetting = new GlobalSetting();
                $globalSetting->logo = "";
                $globalSetting->website_title = $request->title;
                $globalSetting->facebook_url = $request->facebook_url;
                $globalSetting->instagram_url = $request->instagram_url;
                $globalSetting->twitter_url = $request->twitter_url;
                $globalSetting->registration_allowed = $request->registration_allowed == "1" ? true : false;
                $globalSetting->portfolio_order = $request->portfolio_order;
                $globalSetting->personal_order = $request->personal_order;
                $globalSetting->save();

                return redirect('admin')->with('status', 'Settings Added Successfully');
            } else {
                $temp->website_title = $request->title;
                $temp->facebook_url = $request->facebook_url;
                $temp->instagram_url = $request->instagram_url;
                $temp->twitter_url = $request->twitter_url;
                $temp->registration_allowed = $request->registration_allowed == "1" ? true : false;
                $temp->portfolio_order = $request->portfolio_order;
                $temp->personal_order = $request->personal_order;
                $temp->update();

                return redirect('admin')->with('status', 'Settings Updated Successfully');
            }
        }


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUploadImage(Request $request)
    {
        $path = $request->file('image')->store('gallery');

        $image = new Gallery();
        $image->name = $request->file('image')->getClientOriginalName();
        $image->mode = $request->input('mode');
        $image->image_url = $path;

        $image->save();

        return redirect('admin/gallery')->with('status', 'File Uploaded Successfully');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLandingPage(Request $request)
    {
        $landingpage = new LandingPage();
        $landingpage->image_id = $request->input('image');
        $landingpage->status = $request->input('status') == "enable" ? 1 : 0;
        $landingpage->save();

        return redirect('admin/landingpage');
    }

    /**
     * @param Request $request
     * @param $imageid
     * @param $status
     * @param $type
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postStatusChange(Request $request, $imageid, $status, $type)
    {
        /*
         * Type 1 : Landing Page
         * Type 2 : Home Page
         * */
        switch ($type) {
            case 1:
                $landingImage = LandingPage::find($imageid);
                $landingImage->status = !$status;
                $landingImage->update();

                return redirect('admin/landing-page');
                break;
            case 2:
                $homeImage = HomepageSetting::find($imageid);
                $homeImage->status = !$status;
                $homeImage->update();

                return redirect('admin/homepage');
                break;
        }


    }

    /**
     * @param Request $request
     * @param $imageid
     * @param $type
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postDeleteImage(Request $request, $imageid, $type)
    {
        /*
        * Type 1 : Landing Page
        * Type 2 : Home Page
        * Type 3 : Personal
        * Type 4 : Portfolio
        * */
        switch ($type) {
            case 1:
                $landingImage = LandingPage::find($imageid);
                $landingImage->delete();

                return redirect('admin/landing-page');
                break;
            case 2:
                $homeImage = HomepageSetting::find($imageid);
                $homeImage->delete();

                return redirect('admin/homepage');
                break;
            case 3:
                $personalImage = Personal::find($imageid);
                $personalImage->delete();

                return redirect('admin/personal');
                break;
            case 4:
                $portfolioImage = Portfolio::find($imageid);
                $portfolioImage->delete();

                return redirect('admin/portfolio');
                break;
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAboutMeSettings(Request $request)
    {
        $update = true;
        $aboutme = AboutMe::first();

        if (count($aboutme) == 0) {
            $update = false;
            $aboutme = new AboutMe();
        }

        $aboutme->name = $request->input('name');
        $aboutme->work = $request->input('work');
        $aboutme->place = $request->input('place');
        $aboutme->title = $request->input('title');
        $aboutme->phone_number = $request->input('phone');
        $aboutme->email = $request->input('email');
        $aboutme->content = $request->input('content');
        $aboutme->image_id = $request->input('image');
        $aboutme->background_image_id = $request->input('background-image');

        if ($update) {
            $aboutme->update();
        } else {
            $aboutme->save();
        }

        return redirect('admin/about-me')->with('status', 'Profile updated successfully');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postHomePage(Request $request)
    {
        $image = Gallery::findorFail($request->image);

        $homepage = new HomepageSetting();
        $homepage->image_id = $request->input('image');
        $homepage->status = $request->input('status');
        $homepage->mode = $image->mode;
        $homepage->order = $request->input('image');

        $homepage->save();

        return redirect('admin/homepage');
    }

    /**
     * @param Request $request
     * @param $imageid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postDeleteGalleryImage(Request $request, $imageid)
    {
        $result = $this->checkImageUsage($imageid);
        if (!$result) {
            return redirect('admin-gallery')->with('status', 'Image is being used in other page. Delete Image from that page first');
        } else {

            $image = Gallery::find($imageid);
            Storage::delete($image->image_url);
            Storage::delete($image->image_thumbnail_url);
            $image->delete();

            return redirect('admin/gallery')->with('status', 'Image Deleted Successfully');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postPortfolio(Request $request)
    {
        $portfolio = new Portfolio();
        $portfolio->title = $request->input('name');
        $portfolio->category = $request->input('category');
        $portfolio->status = $request->input('status');
        $portfolio->image_id = $request->input('image');

        if ($request->input('order') != "") {
            $portfolio->order = $request->input('order');
        }

        $portfolio->save();

        return redirect('admin/portfolio')->with('status', 'Portfolio Added Successfully');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postPersonal(Request $request)
    {
        $personal = new Personal();
        $personal->title = $request->input('name');
        $personal->category = $request->input('category');
        $personal->status = $request->input('status');
        $personal->image_id = $request->input('image');

        if ($request->input('order') != "") {
            $personal->order = $request->input('order');
        }

        $personal->save();

        return redirect('admin/personal')->with('status', 'Personal portfolio Added Successfully');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdminHomepageEditOrder(Request $request)
    {
        $image = HomepageSetting::find(trim($request->input('imageid')));
        $image->order = $request->input('order');
        $image->mode = $request->input('mode');
        $image->update();

        return redirect('admin/homepage')->with('status', 'Image Order Changed Successfully');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdminPortfolioEditOrder(Request $request)
    {
        $image = Portfolio::find(trim($request->input('imageid')));
        $image->category = $request->input('category');
        $image->order = $request->input('order');
        $image->update();

        return redirect('admin/portfolio')->with('status', 'Image Order Changed Successfully');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdminPersonalEditOrder(Request $request)
    {
        $image = Personal::find(trim($request->input('imageid')));
        $image->category = $request->input('category');
        $image->order = $request->input('order');
        $image->update();

        return redirect('admin/personal')->with('status', 'Image Order Changed Successfully');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdminPortfolioCategory(Request $request)
    {
        /*
         *  Category Type
         * 1. Portfolio
         * 2. Personal
         * */

        $category = new Category();
        $category->name = $request->input('name');
        $category->status = $request->input('status');
        $category->type = 1;
        $category->save();

        return redirect('admin/portfolio')->with('status', 'Category Added Successfully');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdminPersonalCategory(Request $request)
    {
        /*
         *  Category Type
         * 1. Portfolio
         * 2. Personal
         * */
        $category = new Category();
        $category->name = $request->input('name');
        $category->status = $request->input('status');
        $category->type = 2;
        $category->save();

        return redirect('admin/personal')->with('status', 'Category Added Successfully');

    }

    /**
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postStatusChangeCategory($id, $status)
    {
        $category = Category::find($id);

        $category->status = $status == "1" ? "0" : "1";
        $category->update();

        return redirect('admin/categories')->with('status', ' Category Status changed successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDeleteCategory($id)
    {
        $category = Category::find($id);

        if ($category->status == "0") {

            $isCategoryUsed = $this->isCategoryUsed($id);
            if ($isCategoryUsed) {
                $category->delete();

                return redirect('admin/categories')->with('status', 'Category Deleted Successfully');
            }

            return back()->with('status', 'Category is being used in Portfolio/Personal. Cannot Delete!');
        } else {
            return back()->with('status', 'Category is still Active. Please disable it before deleting');
        }

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function uploadImage(Request $request)
    {
        $storeDirectory = "/gallery/";
        $validate = $this->validate($request, [
            'file' => 'image|max:3000'
        ]);

        if ($validate) {
            return Response::make($validate->errors->first(), 400);
        }

        if ($request->hasFile('file')) {
            $file = $request->file('file');
        }

        $originalName = $file->getClientOriginalName();
        $filename = sha1(time() . str_random(5)) . "." . $file->getClientOriginalExtension();
        $image = Image::make($file->getRealPath());
        $imageConfig = $this->checkImageInfo($image);
        $image->save(storage_path('app/public/gallery/' . $filename));
        $thumbnail = $image->fit($imageConfig["width"], $imageConfig["height"]);
        $thumbnail->save(storage_path('app/public/gallery/tn-' . $filename));


        $galleryImage = new Gallery();
        $galleryImage->name = $filename;
        $galleryImage->original_name = $originalName;
        $galleryImage->mode = $imageConfig["mode"];
        $galleryImage->image_url = $storeDirectory . $filename;
        $galleryImage->image_thumbnail_url = $storeDirectory . "tn-" . $filename;


        if ($galleryImage->save()) {
            return response()->json([
                'success' => 200
            ]);
        } else {
            return response()->json([
                'error' => 400
            ]);
        }
    }

    /**
     * @param $image
     * @return array
     */
    private function checkImageInfo($image)
    {

        /*
         * Check width/height/proportions
         * Save the mode,main image and thumbnail
         * Portrait (683px X 1024px)
         * Landscape (1920px X 1080px)
         * Portfolio Cover - Landscape (475px X 370px)
         * Portfolio Cover - Portrait (475px X 774px)
         * Portfolio Main Image (Any Resolution)
         * */
        $width = $image->width();
        $height = $image->height();

        if ($width == 683 && $height == 1024)
            return [
                'mode' => 1,
                'width' => 475,
                'height' => 774
            ];
        elseif ($width == 1920 && $height == 1080)
            return [
                'mode' => 2,
                'width' => 475,
                'height' => 370
            ];
        else
            return [
                'mode' => 3
            ];
    }

    /**
     * @param $imageid
     * @return bool
     */
    private function checkImageUsage($imageid)
    {
        if (count(LandingPage::where('image_id', $imageid)->get()) > 0) {
            return false;
        } else if (count(HomepageSetting::where('image_id', $imageid)->get()) > 0) {
            return false;
        } else if (count(AboutMe::where('image_id', $imageid)->get()) > 0) {
            return false;
        } else if (count(AboutMe::where('background_image_id', $imageid)->get()) > 0) {
            return false;
        } else if (count(Portfolio::where('image_id', $imageid)->get()) > 0) {
            return false;
        } else if (count(PersonalGallery::where('image_id', $imageid)->get()) > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getGallery()
    {
        return Gallery::all();
    }

    /**
     * @param $id
     * @return bool
     */
    private function isCategoryUsed($id)
    {
        $category = Category::find($id);

        if ($category->personalPhotos->count() > 0 || $category->portfolioPhotos->count() > 0) {
            return false;
        } else {
            return true;
        }
    }
}
