<?php

namespace App\Http\Middleware;

use Closure;
use App\GlobalSetting;

class checkRegisterAllowed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $globalSetting = GlobalSetting::first();

        if (count($globalSetting) == 0){
            return $next($request);
        } else {
            if ($globalSetting->registration_allowed == 1)
                return $next($request);
            else
                return redirect('login')->with('status', 'Registration is disabled');
        }
    }
}
