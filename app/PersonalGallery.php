<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalGallery extends Model
{
    //
    function gallery()
    {
        return $this->hasOne(Gallery::class, 'id', 'image_id');
    }
}
