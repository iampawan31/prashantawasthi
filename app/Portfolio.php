<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{

    function categoryDetail()
    {
        return $this->belongsTo(Category::class, 'category', 'id');
    }

    function mainImageGallery()
    {
        return $this->belongsTo(Gallery::class, 'image_id', 'id');
    }
}
