<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomepageSetting extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    function gallery(){
        return $this->hasOne(Gallery::class, 'id', 'image_id');
    }
    
}
