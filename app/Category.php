<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function personalPhotos()
    {
        return $this->hasMany(Personal::class, 'category' ,'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function portfolioPhotos()
    {
        return $this->hasMany(Portfolio::class, 'category' ,'id');
    }
}
