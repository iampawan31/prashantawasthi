<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalSetting extends Model
{
    public function samplePortfolioOrder($value)
    {
        /*
         * 1. Name (ASC)
           2. Name (DESC)
           3. Order (ASC)
           4. Order (DESC)
           5. Uploaded (ASC)
           6. Uploaded (DESC)
        */

        if ($value == 1) {
            return "Name (ASC)";
        } else if ($value == 2) {
            return "Name (DESC)";
        } else if ($value == 3) {
            return "Order (ASC)";
        } else if ($value == 4) {
            return "Order (DESC)";
        } else if ($value == 5) {
            return "Uploaded (ASC)";
        } else if ($value == 6) {
            return "Uploaded (DESC)";
        }
    }

    public function samplePersonalOrder($value)
    {
        /*
         * 1. Name (ASC)
           2. Name (DESC)
           3. Order (ASC)
           4. Order (DESC)
           5. Uploaded (ASC)
           6. Uploaded (DESC)
        */

        if ($value == 1) {
            return "Name (ASC)";
        } else if ($value == 2) {
            return "Name (DESC)";
        } else if ($value == 3) {
            return "Order (ASC)";
        } else if ($value == 4) {
            return "Order (DESC)";
        } else if ($value == 5) {
            return "Uploaded (ASC)";
        } else if ($value == 6) {
            return "Uploaded (DESC)";
        }
    }
}
