<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function mainImageGallery()
    {
        return $this->belongsTo(Gallery::class, 'image_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function categoryDetail()
    {
        return $this->belongsTo(Category::class, 'category', 'id');
    }
}
