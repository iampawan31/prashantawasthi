<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendContactFormSubmitted extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.ContactFormSubmitted')
            ->with([
                'name' => $this->data->user_name,
                'email' => $this->data->user_email,
                'subject' => $this->data->message_subject,
                'message_content' => $this->data->message_content
            ]);
    }
}
