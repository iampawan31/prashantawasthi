<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutMe extends Model
{
    /**
     * @var string
     */
    protected $table = "aboutme";


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function aboutMeProfilePicture(){
        return $this->belongsTo(Gallery::class, 'image_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function aboutMeBackgroundPicture(){
        return $this->belongsTo(Gallery::class, 'background_image_id', 'id');
    }
}
