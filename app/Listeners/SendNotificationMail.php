<?php

namespace App\Listeners;

use App\Events\ContactFormSubmitted;
use App\Mail\SendContactFormSubmitted;
use Illuminate\Support\Facades\Mail;

class SendNotificationMail
{

    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  ContactFormSubmitted  $event
     * @return void
     */
    public function handle(ContactFormSubmitted $event)
    {
        Mail::to(config('mail.from.address'))->send(new SendContactFormSubmitted($event->request));
    }
}
