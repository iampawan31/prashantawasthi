<?php

namespace App\Listeners;

use App\ContactForm;
use App\Events\ContactFormSubmitted;

class AddToDatabase
{
    /**
     * Create the event listener.
     *
     * */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  ContactFormSubmitted  $event
     * @return void
     */
    public function handle(ContactFormSubmitted $event)
    {
        $contactForm = new ContactForm();
        $contactForm->name = $event->request->user_name;
        $contactForm->email = $event->request->user_email;
        $contactForm->subject= $event->request->message_subject;
        $contactForm->message_content = $event->request->message_content;
        $contactForm->save();
    }
}
